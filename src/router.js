import Vue from "vue";
import VueRouter from "vue-router";

import HomePage from "./pages/HomePage";
import ContactPage from "./pages/ContactPage";
import AboutUsPage from "./pages/AboutUsPage";
import BlogDetail from "./pages/BlogDetailPage";
import Login from "./pages/LoginPage";
import NotFound from "./pages/NotFound";
import store from "./store";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    component: HomePage,
    name: "home-page",
  },
  {
    path: "/contact",
    component: ContactPage,
    name: "contact",
  },
  {
    path: "/blog-detail/:title:id",
    component: BlogDetail,
    name: "blog-detail",
    // Chặn người dùng, khi người dùng chưa đăng nhập thì sẽ không xem được blog
    // và chuyển người dùng sang trang login
    beforeEnter: (to, from, next) => {
      if (store.state.user.isLogin) {
        next();
      } else {
        next({ path: "/login", query: { redirect: "blog-detail" } });
      }
    },
  },
  {
    path: "/about-us",
    component: AboutUsPage,
    name: "about-us",
  },
  {
    path: "/login",
    name: "login-page",
    component: Login,
  },
  {
    path: "*",
    name: "not-found",
    component: NotFound,
  },
];

const router = new VueRouter({
  mode: "history",
  routes,
});

export default router;
