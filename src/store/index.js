import Vue from "vue";
import Vuex from "vuex";
import state from "./state";
import mutations from "./mutations";
import getters from "./getters";
import actions from "./actions";
import moduleBlogs from "./blogs";
import moduleUser from "./user";

Vue.use(Vuex);

const store = new Vuex.Store({
  namespaced: true,
  state,
  mutations,
  getters,
  actions,
  modules: {
    blogs: moduleBlogs,
    user: moduleUser,
  },
});

export default store;
