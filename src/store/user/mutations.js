export default {
  CHECK_LOGIN(state, isLogin = false) {
    state.isLogin = isLogin;
  },
  LOGOUT(state, isLogin = false) {
    state.isLogin = isLogin;
  },
};
