import state from "../blogs/state";
import mutations from "../blogs/mutations";
import getters from "../blogs/getters";
import actions from "../blogs/actions";

export default {
  namespaced: true,
  state,
  mutations,
  getters,
  actions,
};
