import Vue from "vue";
import App from "./App.vue";

Vue.config.productionTip = false;

import "./plugins/bootstrap";
import router from "./router";
import blogs from "./mocks/blog-list";
import store from "./store/index";

new Vue({
  router,
  blogs,
  store,
  render: (h) => h(App),
}).$mount("#app");
