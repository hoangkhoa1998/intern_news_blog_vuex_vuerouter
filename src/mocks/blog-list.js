import { faker } from "@faker-js/faker";

const listBlogs = [];

console.log(faker);

for (let index = 0; index < 10; index++) {
  const data = {
    id: Math.round(Math.random() * 999),
    title: faker.lorem.sentence(10),
    abstract: faker.lorem.paragraph(2),
    paragraph: faker.lorem.text(),
    fullName: faker.name.firstName() + " " + faker.name.lastName(),
    date: faker.date.past(),
    image: faker.image.nature(),
  };
  listBlogs.push(data);
}

export default listBlogs;
